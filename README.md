# INTRODUCTION

The Translation String Import module provides drush commands and
an API to add UI translations.

- For a full description of the module, visit the project page:
  https://www.drupal.org/project/tsi

- To submit bug reports and feature suggestions, or to track changes:
  https://www.drupal.org/project/issues/tsi

## INSTALLATION

- Install as you would normally install a contributed Drupal module. Visit
  https://www.drupal.org/node/1897420 for further information.

## USAGE

Translation String Import command needs the following parameters:

- source: the source string, you want to translate.
- translated: the translated string.
- langcode: the target langcode.
- context: context of the translation (optional).

### Drush

Add a single 'singular' string translation:
``drush tsi "example" "Beispiel" "de"``

Add a single 'plural' string translation:
``drush tpi "1 example" "1 Beispiel" "@count examples" "@count Beispiele" "de"``

### Deploy Hook single translation

In a deploy hook use it like this for a single 'singular' translation:

```
function mymodule_deploy_singular_xy() {
  \Drupal::service('tsi.service')->addTranslation('example', 'Beispiel', 'de');
}
```

In a deploy hook for a single 'plural' translation we use this:

```
function mymodule_deploy_plural_xy() {
  \Drupal::service('tsi.service')->addPluralTranslation('1 example', '1 Beispiel', '@count examples', '@count Beispiele', 'de', '');
}
```

### Deploy Hook multiple translation

In a deploy hook for multiple translation we need to create an array and hand it over to the service:

This is how it looks for singular case:

```
function mymodule_deploy_singular_mutliple_xy() {
  $translations_array = [
    'cat' => 'Katze',
    'dog' => 'Hund'
  ];

  \Drupal::service('tsi.service')->addMultipleTranslation($translations_array, 'de');

  return t('Deploy hook triggered');
}
```

This is how it looks for plural case:

```

function mymodule_deploy_plural_multiple_xy() {
  $translation_array = [
    ['1 cat', '1 Katze', '@count cats', '@count Katzen'],
    ['1 dog', '1 Hund', '@count dogs', '@count Hunde']
  ];

  \Drupal::service('tsi.service')->addMultiplePluralTranslation($translation_array, 'de', '');
  return t('Deploy hook triggered');
}
```
