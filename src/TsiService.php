<?php

namespace Drupal\tsi;

use Drupal\Component\Gettext\PoItem;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\locale\SourceString;
use Drupal\locale\StringStorageInterface;

/**
 * Translation String Import Service.
 *
 * Import Translation Strings directly.
 */
class TsiService implements TsiServiceInterface {

  use StringTranslationTrait;

  /**
   * String translation storage object.
   *
   * @var \Drupal\locale\StringStorageInterface
   */
  protected $localeStorage;

  /**
   * Constructs a new TsiService object.
   */
  public function __construct(StringStorageInterface $locale_storage) {
    $this->localeStorage = $locale_storage;
  }

  /**
   * {@inheritDoc}
   */
  public function addTranslation($source, $translated, $langcode, $context = ''): void {
    $string = $this->createStringTranslation($source, $translated, $langcode, $context);
    $this->clearLocaleCaches($langcode, [$string->lid]);
  }

  /**
   * {@inheritDoc}
   */
  public function addMultipleTranslation(array $translation_array, string $langcode, $context = ''): void {
    $lids = [];
    foreach ($translation_array as $source => $translated) {
      $string = $this->createStringTranslation($source, $translated, $langcode, $context);
      $lids[] = $string->lid;
    }

    $this->clearLocaleCaches($langcode, $lids);
  }

  /**
   * Call of the old method.
   *
   * Redirect the deprecated method to the updated method.
   */
  public function addPlural($singular_source, $singular_translated, $plural_source, $plural_translated, $langcode, $context = ''): void {
    $this->addPluralTranslation($singular_source, $singular_translated, $plural_source, $plural_translated, $langcode, $context);
  }

  /**
   * {@inheritdoc}
   */
  public function addPluralTranslation($singular_source, $singular_translated, $plural_source, $plural_translated, $langcode, $context = ''): void {
    $string = $this->createPluralStringTranslation($singular_source, $singular_translated, $plural_source, $plural_translated, $langcode, $context);
    $this->clearLocaleCaches($langcode, [$string->lid]);
  }

  /**
   * {@inheritDoc}
   */
  public function addMultiplePluralTranslation(array $translation_array, string $langcode, $context = ''): void {
    $lids = [];
    foreach ($translation_array as $translations) {
      // Check if the array has the correct length.
      assert(count($translations) === 4, $this->t('Translation array must contain exactly 4 elements.'));

      [$singular_source,
        $singular_translated,
        $plural_source,
        $plural_translated,
      ] = $translations;

      $string = $this->createPluralStringTranslation($singular_source, $singular_translated, $plural_source, $plural_translated, $langcode, $context);
      $lids[] = $string->lid;
    }

    $this->clearLocaleCaches($langcode, $lids);
  }

  /**
   * Creates a new SourceString object or update it with a translation.
   *
   * @param string $source
   *   The string in the default language you want to translate.
   * @param string $translated
   *   The translation of $source into the language given through the $langcode.
   * @param string $langcode
   *   A short hint which language you use for translation (de, en, es, etc.).
   * @param string $context
   *   A separate key to differ the translations.
   *
   * @throws \Drupal\locale\StringStorageException
   */
  protected function createStringTranslation($source, $translated, $langcode, $context): SourceString {
    $string = $this->localeStorage->findString(
      [
        'source' => $source,
        'context' => $context,
      ]
    );

    if (is_null($string)) {
      $string = new SourceString();
      $string->setString($source);
      $string->setStorage($this->localeStorage);
      $string->context = $context;
      $string->save();
    }

    $this->localeStorage->createTranslation(
      [
        'lid' => $string->lid,
        'language' => $langcode,
        'translation' => $translated,
      ]
    )->save();

    return $string;
  }

  /**
   * Creates a new SourceString object or update it with a plural translation.
   *
   * @param string $singular_source
   *   The singular form of the source string in default language.
   * @param string $singular_translated
   *   The singular form of the translated source string in the language given
   *   by the $langcode.
   * @param string $plural_source
   *   The plural form of the source string in default language.
   * @param string $plural_translated
   *   The plural form of the translated source string in the language given by
   *   the $langcode.
   * @param string $langcode
   *   The language code for the translations.
   * @param string $context
   *   (optional) The context for the translations.
   *
   * @throws \Drupal\locale\StringStorageException
   */
  protected function createPluralStringTranslation($singular_source, $singular_translated, $plural_source, $plural_translated, $langcode, $context = ''): SourceString {
    $string = $this->localeStorage->findString(
      [
        'source' => implode(
          PoItem::DELIMITER, [
            $singular_source,
            $plural_source,
          ]
        ),
        'context' => $context,
      ]
    );

    if (is_null($string)) {
      $string = new SourceString();
      $string->setPlurals([$singular_source, $plural_source]);
      $string->setStorage($this->localeStorage);
      $string->context = $context;
      $string->save();
    }

    if ($translations = $this->localeStorage->getTranslations(
      [
        'lid' => $string->lid,
        'language' => $langcode,
        'translated' => TRUE,
      ]
    )
    ) {
      $translation = reset($translations);
    }
    else {
      $translation = $this->localeStorage->createTranslation(
        [
          'lid' => $string->lid,
          'language' => $langcode,
        ]
      );
    }
    $translation->setPlurals([$singular_translated, $plural_translated]);
    $translation->save();

    return $string;
  }

  /**
   * Clears the locale caches for a given language and list of locale IDs.
   *
   * @param string $langcode
   *   The language code for which to clear the caches.
   * @param array $lids
   *   An array of locale IDs to clear from the caches.
   */
  protected function clearLocaleCaches($langcode, array $lids): void {
    _locale_invalidate_js($langcode);
    _locale_refresh_translations([$langcode], $lids);
    _locale_refresh_configuration([$langcode], $lids);
  }

}
