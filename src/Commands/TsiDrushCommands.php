<?php

namespace Drupal\tsi\Commands;

use Drush\Commands\DrushCommands;
use Drupal\tsi\TsiServiceInterface;

/**
 * A drush command file.
 *
 * @package Drupal\tsi\Commands
 */
class TsiDrushCommands extends DrushCommands {

  /**
   * The tsi service.
   *
   * @var \Drupal\tsi\TsiServiceInterface
   */
  protected $tsiService;

  /**
   * Constructs a new TsiService object.
   */
  public function __construct(TsiServiceInterface $tsi_service) {
    parent::__construct();
    $this->tsiService = $tsi_service;
  }

  /**
   * Drush command that imports a translation string.
   *
   * @param string $source
   *   Source string.
   * @param string $translated
   *   Translated string.
   * @param string $langcode
   *   Language (f.e 'de').
   * @param string $context
   *   The context of the translation (default = '').
   *
   * @command tsi:add
   * @aliases tsi
   * @usage tsi "Hello" "Hallo" de
   */
  public function tsi($source, $translated, $langcode, $context = '') {
    $this->tsiService->addTranslation($source, $translated, $langcode, $context);
    $this->output()
      ->writeln("Translation added [$source -> $translated] (Language: $langcode, Context: $context)");
  }

  /**
   * Drush command that imports a translation string.
   *
   * @param string $singular_source
   *   The singular form of the source string in default language.
   * @param string $singular_translated
   *   The singular form of the translated source string in the language given
   *   by the $langcode.
   * @param string $plural_source
   *   The plural form of the source string in default language.
   * @param string $plural_translated
   *   The plural form of the translated source string in the language given by
   *   the $langcode.
   * @param string $langcode
   *   The language code for the translations.
   * @param string $context
   *   (optional) The context for the translations.
   *
   * @command tpi:add
   * @aliases tpi
   * @usage tsi "1 potato" "@count potatoes" "1 Kartoffel" "@count Kartoffeln" de
   */
  public function tpi($singular_source, $singular_translated, $plural_source, $plural_translated, $langcode, $context = '') {
    $this->tsiService->addPluralTranslation($singular_source, $singular_translated, $plural_source, $plural_translated, $langcode, $context);
    $this->output()->writeln("Translation added [$singular_source -> $singular_translated, $plural_source -> $plural_translated] (Language: $langcode, Context: $context)");
  }

}
