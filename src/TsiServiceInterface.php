<?php

namespace Drupal\tsi;

/**
 * Defines the TsiServiceInterface.
 */
interface TsiServiceInterface {

  /**
   * Adds a translation.
   *
   * @param string $source
   *   The source string.
   * @param string $translated
   *   The translated string.
   * @param string $langcode
   *   The language code (e.g. 'de').
   * @param string $context
   *   The context of the translation (default = '')
   */
  public function addTranslation($source, $translated, $langcode, $context = ''): void;

  /**
   * Adds translations for multiple source strings.
   *
   * @param array $translation_array
   *   An array of source string and translated string pairs.
   * @param string $langcode
   *   The language code for the translations.
   * @param string $context
   *   (optional) The context for the translations.
   *
   * @throws \Drupal\locale\StringStorageException
   */
  public function addMultipleTranslation(array $translation_array, string $langcode, $context = ''): void;

  /**
   * Adds translation for plural strings.
   *
   * @param string $singular_source
   *   The singular form of the source string in default language.
   * @param string $singular_translated
   *   The singular form of the translated source string in the language given
   *   by the $langcode.
   * @param string $plural_source
   *   The plural form of the source string in default language.
   * @param string $plural_translated
   *   The plural form of the translated source string in the language given by
   *   the $langcode.
   * @param string $langcode
   *   The language code for the translations.
   * @param string $context
   *   (optional) The context for the translations.
   *
   * @depricated Use addPluralTranslation() instead.
   */
  public function addPlural($singular_source, $singular_translated, $plural_source, $plural_translated, $langcode, $context = '');

  /**
   * Adds translation for plural strings.
   *
   * @param string $singular_source
   *   The singular form of the source string in default language.
   * @param string $singular_translated
   *   The singular form of the translated source string in the language given
   *   by the $langcode.
   * @param string $plural_source
   *   The plural form of the source string in default language.
   * @param string $plural_translated
   *   The plural form of the translated source string in the language given by
   *   the $langcode.
   * @param string $langcode
   *   The language code for the translations.
   * @param string $context
   *   (optional) The context for the translations.
   */
  public function addPluralTranslation($singular_source, $singular_translated, $plural_source, $plural_translated, $langcode, $context = ''): void;

  /**
   * Adds translations for multiple plural strings.
   *
   * @param array $translation_array
   *   The data structure is a multidimensional array consisting of an array
   *   with.
   * @param string $langcode
   *   The langcode.
   * @param string $context
   *   The context of the translation (default = '')
   */
  public function addMultiplePluralTranslation(array $translation_array, string $langcode, $context = ''): void;

}
