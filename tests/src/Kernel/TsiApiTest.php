<?php

namespace Drupal\Tests\tsi\Kernel;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\KernelTests\KernelTestBase;
use Drupal\language\Entity\ConfigurableLanguage;

/**
 * Class for Tsi Api Test.
 */
class TsiApiTest extends KernelTestBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'tsi',
    'language',
    'locale',
  ];

  /**
   * The TSI service.
   *
   * @var \Drupal\tsi\TsiServiceInterface
   */
  protected $tsiService;

  /**
   * The translation service.
   *
   * @var \Drupal\Core\StringTranslation\TranslationInterface
   */
  protected $translationService;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Initialize the TSI service.
    $this->tsiService = $this->container->get('tsi.service');

    // Get the string_translation service.
    $this->translationService = $this->container->get('string_translation');

    // Create two languages: Spanish and German.
    foreach (['es', 'de'] as $langcode) {
      ConfigurableLanguage::createFromLangcode($langcode)->save();
    }
    $this->installSchema(
      'locale', [
        'locales_location',
        'locales_source',
        'locales_target',
      ]
    );

    // Install required modules and perform other setup tasks.
    $this->installConfig('locale');
    // Additional setup tasks specific to your module.
  }

  /**
   * Tests addTranslation().
   */
  public function testAddTranslation() {
    // Call the addTranslation() method with some test data.
    $source = 'Hello';
    $translated = 'Hallo';
    $langcode = 'de';
    $this->tsiService->addTranslation($source, $translated, $langcode);

    // Assert that the translated string is correct.
    $this->assertEquals($translated, $this->t($source, [], ['langcode' => $langcode]));
  }

  /**
   * Tests addMultipleTranslation().
   */
  public function testAddMultipleTranslation() {
    // Define the translation array.
    $translation_array = [
      'Hello' => 'Hallo',
      'Goodbye' => 'Auf Wiedersehen',
      'Thank you' => 'Danke schön',
    ];
    // Define the language code.
    $langcode = 'de';

    // Call the method under test.
    $this->tsiService->addMultipleTranslation($translation_array, $langcode);

    // Assert that the translations were added successfully.
    foreach ($translation_array as $source => $translated) {
      $this->assertEquals($translated, $this->t($source, [], ['langcode' => $langcode]));
    }
  }

  /**
   * Tests addPluralTranslation() with an already existing translation.
   */
  public function testAddPluralTranslationWithExistingPlural() {
    $this->assertEquals('1 example', $this->formatPlural(1, '1 example', '@count examples', [], ['langcode' => 'de']));
    $this->assertEquals('2 examples', $this->formatPlural(2, '1 example', '@count examples', [], ['langcode' => 'de']));

    // Add the plural translation.
    $this->tsiService->addPluralTranslation('1 example', '1 Beispiel', '@count examples', '@count Beispiele', 'de', '');
    // Reset the service static cache.
    \Drupal::service('string_translation')->reset();

    // Assert the updated translation value.
    $this->assertEquals('1 Beispiel', $this->formatPlural(1, '1 example', '@count examples', [], ['langcode' => 'de']));
    $this->assertEquals('2 Beispiele', $this->formatPlural(2, '1 example', '@count examples', [], ['langcode' => 'de']));

  }

  /**
   * Tests addPluralTranslation() without an already existing translation.
   */
  public function testAddPluralTranslationWithoutExistingPlural() {
    // Add the plural translation.
    $this->tsiService->addPluralTranslation('1 example', '1 Beispiel', '@count examples', '@count Beispiele', 'de', '');
    // Assert the updated translation value.
    $this->assertEquals('1 Beispiel', $this->formatPlural(1, '1 example', '@count examples', [], ['langcode' => 'de']));
    $this->assertEquals('2 Beispiele', $this->formatPlural(2, '1 example', '@count examples', [], ['langcode' => 'de']));
  }

  /**
   * Tests addMultiplePluralTranslation() with an already existing translation.
   */
  public function testAddMultiplePluralTranslationWithExistingPlural() {
    $this->assertEquals('1 example', $this->formatPlural(1, '1 example', '@count examples', [], ['langcode' => 'de']));
    $this->assertEquals('2 examples', $this->formatPlural(2, '1 example', '@count examples', [], ['langcode' => 'de']));
    $this->assertEquals('1 site', $this->formatPlural(1, '1 site', '@count sites', [], ['langcode' => 'de']));
    $this->assertEquals('2 sites', $this->formatPlural(2, '1 site', '@count sites', [], ['langcode' => 'de']));

    $translation_array = [
      ['1 example', '1 Beispiel', '@count examples', '@count Beispiele'],
      ['1 site', '1 Seite', '@count sites', '@count Seiten'],
    ];

    // Add the plural translation.
    $this->tsiService->addMultiplePluralTranslation($translation_array, 'de', '');
    // Reset the service static cache.
    \Drupal::service('string_translation')->reset();

    // Assert the updated translation value.
    $this->assertEquals('1 Beispiel', $this->formatPlural(1, '1 example', '@count examples', [], ['langcode' => 'de']));
    $this->assertEquals('2 Beispiele', $this->formatPlural(2, '1 example', '@count examples', [], ['langcode' => 'de']));
    $this->assertEquals('1 Seite', $this->formatPlural(1, '1 site', '@count sites', [], ['langcode' => 'de']));
    $this->assertEquals('2 Seiten', $this->formatPlural(2, '1 site', '@count sites', [], ['langcode' => 'de']));

  }

  /**
   * Tests addMultiplePluralTranslation() with an already existing translation.
   */
  public function testAddMultiplePluralTranslationWithoutExistingPlural() {
    $translation_array = [
      ['1 example', '1 Beispiel', '@count examples', '@count Beispiele'],
      ['1 site', '1 Seite', '@count sites', '@count Seiten'],
    ];

    // Add the plural translation.
    $this->tsiService->addMultiplePluralTranslation($translation_array, 'de', '');

    // Assert the updated translation value.
    $this->assertEquals('1 Beispiel', $this->formatPlural(1, '1 example', '@count examples', [], ['langcode' => 'de']));
    $this->assertEquals('2 Beispiele', $this->formatPlural(2, '1 example', '@count examples', [], ['langcode' => 'de']));
    $this->assertEquals('1 Seite', $this->formatPlural(1, '1 site', '@count sites', [], ['langcode' => 'de']));
    $this->assertEquals('2 Seiten', $this->formatPlural(2, '1 site', '@count sites', [], ['langcode' => 'de']));

  }

  /**
   * Tests the array length check.
   */
  public function testAddMultiplePluralTranslationWithWrongArrayLength() {
    // Creating wrong arrays to trigger error.
    $translation_array = [
      ['1 example', '1 Beispiel', '@count examples'],
      ['1 site', '1 Seite', '@count sites'],
    ];

    // Assert the updated translation will throw an error.
    $this->expectException(\AssertionError::class);
    $this->expectExceptionMessage('Translation array must contain exactly 4 elements.');

    // Add the plural translation.
    $this->tsiService->addMultiplePluralTranslation($translation_array, 'de', '');

  }

}
